// OEdit.cpp: implementation of the OEdit class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
OEdit::OEdit ()
{
	m_Caret.bShow	= FALSE;
	m_Caret.bFlash	= FALSE;
	m_Caret.bValid	= FALSE;
	m_Caret.lTimeInterval = 1;
	m_Caret.x		= 0;
	m_Caret.y		= 0;
	m_Caret.w		= 0;
	m_Caret.h		= 0;

	m_cIMEStatus	= IME_FORBID;   // 输入法状态；0禁用输入法；1输入法未开；2输入法已开

	m_dwAddData1	= 0xFFFFFFFF;   // 插入起始位置
	m_dwAddData2	= 0xFFFFFFFF;   // 插入终止位置
	m_dwAddData3	= 255;		// 字符串默认最大长度
	m_dwAddData4	= 0;		// 显示区域左端的第一个字符的位置

#if defined (CHINESE_SUPPORT)
	m_pIME			= NULL;
#endif //defined(CHINESE_SUPPORT)
}

OEdit::~OEdit ()
{
#if defined (CHINESE_SUPPORT)
	m_pIME = NULL;			// m_pIME是全局的，不能删除！！！
#endif // defined(CHINESE_SUPPORT)
}

// 创建编辑框
BOOL OEdit::Create (OWindow* pParent,WORD wStyle,WORD wStatus,int x,int y,int w,int h,int ID)
{
	// 如果禁用输入法，则将m_cIMEStatus设置为0
	if ((wStyle & WND_STYLE_DISABLE_IME) > 0) {
		m_cIMEStatus = IME_FORBID;
	}
	else {
		m_cIMEStatus = IME_CLOSE;
	}

	if (! OWindow::Create (pParent,self_type,wStyle,wStatus,x,y,w,h,ID)) {
		return FALSE;
	}

	return TRUE;
}

// 虚函数，绘制编辑框
void OEdit::Paint (LCD* pLCD)
{
	CHECK_TYPE;

	// 如果不可见，则什么也不绘制
	if (! IsWindowVisible()) {
		return;
	}

	int nLeftPos  = m_dwAddData4;
	int nRightPos = GetRightDisplayIndex ();
	int crBk = 1;
	int crFr = 0;

	// 绘制边框，填充背景
	pLCD->FillRect (m_x, m_y, m_w, m_h, crBk);
	if ((m_wStyle & WND_STYLE_NO_BORDER) == 0)
	{
		pLCD->HLine (m_x, m_y, m_w, crFr);
		pLCD->HLine (m_x, m_y+m_h-1, m_w, crFr);
		pLCD->VLine (m_x, m_y, m_h, crFr);
		pLCD->VLine (m_x+m_w-1, m_y, m_h, crFr);
	}

	// 绘制文字
	if ((m_wStyle & WND_STYLE_PASSWORD) == 0)
	{
		// 正常模式
		pLCD->TextOut (m_x+2,m_y+2,(BYTE*)(m_sCaption+nLeftPos),(nRightPos-nLeftPos), LCD_MODE_NORMAL);
	}
	else
	{
		// 密码模式
		char cTemp [256];
		memset (cTemp, 0x0, 256);
		strncpy (cTemp, (m_sCaption+nLeftPos), (nRightPos-nLeftPos));
		memset (cTemp, '*', (nRightPos-nLeftPos));
		pLCD->TextOut (m_x+2,m_y+2,(BYTE*)cTemp,(nRightPos-nLeftPos), LCD_MODE_NORMAL);
	}
}

// 虚函数，消息处理
// 消息处理过了，返回1，未处理返回0
int OEdit::Proc (OWindow* pWnd, int nMsg, int wParam, int lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int nReturn = 0;

#if defined (CHINESE_SUPPORT)
	// 判断IME窗口是否可以处理控制键
	BOOL bIMECanHandleControlKey = FALSE;
	if (m_pIME != NULL) {
		bIMECanHandleControlKey = m_pIME->CanHandleControlKey();
	}
#endif // defined(CHINESE_SUPPORT)

	// 对按键消息的处理
	if (nMsg == OM_KEYDOWN)
	{
		switch (wParam)
		{
		case KEY_TAB:
		case KEY_CAPS_LOCK:
			break;

		case KEY_ESCAPE:
			{

#if defined (CHINESE_SUPPORT)
				// 如果IME处于打开状态，则传给IME窗口处理，并修改iReturn为1
				if (m_cIMEStatus == IME_OPEN)
				{
					if ((m_pIME->m_nCurInputLine == 0) ||
					    (m_pIME->m_nCurIME == 5))
					{
						// 关闭IME窗口
				 		if (m_pApp->CloseIME(this)) {
							m_cIMEStatus = IME_CLOSE;
						}

						m_pIME = NULL;
					}
					else
					{
						O_MSG msg;
						msg.pWnd	= m_pIME;
						msg.message = OM_KEYDOWN;
						msg.wParam	= KEY_ESCAPE;
						msg.lParam	= 0;
						m_pApp->PostMsg (&msg);
					}
					nReturn = 1;
				}
#endif // defined(CHINESE_SUPPORT)

			}
			break;

		case KEY_UP:
			{

#if defined (CHINESE_SUPPORT)
				// 如果IME处于打开状态，则将此消息发送给IME窗口
				if (m_cIMEStatus == IME_OPEN)
				{
					O_MSG msg;
					msg.pWnd	= m_pIME;
					msg.message = OM_KEYDOWN;
					msg.wParam	= KEY_UP;
					msg.lParam	= 0;
					m_pApp->PostMsg (&msg);
					nReturn = 1;
				}
#endif // defined(CHINESE_SUPPORT)

			}
			break;

		case KEY_DOWN:
			{

#if defined (CHINESE_SUPPORT)
				// 如果IME处于打开状态，则将此消息发送给IME窗口
				if (m_cIMEStatus == IME_OPEN)
				{
					O_MSG msg;
					msg.pWnd	= m_pIME;
					msg.message = OM_KEYDOWN;
					msg.wParam	= KEY_DOWN;
					msg.lParam	= 0;
					m_pApp->PostMsg (&msg);
					nReturn = 1;
				}
#endif // defined(CHINESE_SUPPORT)

			}
			break;

		case KEY_LEFT:			// 将当前输入位置向前移动一个字符
			{

#if defined (CHINESE_SUPPORT)
				// 如果IME处于打开状态，且可以处理控制按键，则将此消息发送给IME窗口
				if ((m_cIMEStatus == IME_OPEN) && bIMECanHandleControlKey)
				{
					O_MSG msg;
					msg.pWnd	= m_pIME;
					msg.message = OM_KEYDOWN;
					msg.wParam	= KEY_LEFT;
					msg.lParam	= 0;
					m_pApp->PostMsg (&msg);
				}
				else
				{
#endif // defined(CHINESE_SUPPORT)

					int nStartPos = m_dwAddData1;
					int nEndPos   = m_dwAddData2;

					if ((nStartPos != -1) && (nEndPos != -1))
					{
						if (nStartPos != nEndPos)
						{
							nEndPos = nStartPos;
						}
						else
						{
							if (nStartPos > 0)
							{
								if (IsChinese (m_sCaption, nStartPos, FALSE)) {
									nStartPos -= 2;
								}
								else {
									nStartPos -= 1;
								}

								nEndPos = nStartPos;
							}
						}
						SetSel (nStartPos, nEndPos);
					}

#if defined (CHINESE_SUPPORT)
				}
#endif // defined(CHINESE_SUPPORT)

				nReturn = 1;
			}
			break;

		case KEY_RIGHT:			// 将当前输入位置向后移动一个字符
			{

#if defined (CHINESE_SUPPORT)
				// 如果IME处于打开状态，且可以处理控制按键，则将此消息发送给IME窗口
				if ((m_cIMEStatus == IME_OPEN) && bIMECanHandleControlKey)
				{
					O_MSG msg;
					msg.pWnd	= m_pIME;
					msg.message = OM_KEYDOWN;
					msg.wParam	= KEY_RIGHT;
					msg.lParam	= 0;
					m_pApp->PostMsg (&msg);
				}
				else
				{
#endif // defined(CHINESE_SUPPORT)

					int nStartPos = m_dwAddData1;
					int nEndPos   = m_dwAddData2;

					if ((nStartPos != -1) && (nEndPos != -1))
					{
						if (nStartPos != nEndPos)
						{
							nStartPos = nEndPos;
						}
						else
						{
							if (nStartPos < GetTextLength())
							{
								if (IsChinese (m_sCaption, nStartPos, TRUE)) {
									nStartPos += 2;
								}
								else {
									nStartPos += 1;
								}

								nEndPos = nStartPos;
							}
						}
						SetSel (nStartPos, nEndPos);
					}

#if defined (CHINESE_SUPPORT)
				}
#endif // defined(CHINESE_SUPPORT)

				nReturn = 1;
			}
			break;
		case KEY_HOME:			// 将当前插入位置设置在最前
			{

#if defined (CHINESE_SUPPORT)
				// 如果IME处于关闭状态，或者IME窗口中没有需要编辑的字符，则处理此消息
				if (! ((m_cIMEStatus == IME_OPEN) && bIMECanHandleControlKey))
				{
#endif // defined(CHINESE_SUPPORT)

					SetSel (0, 0);

#if defined (CHINESE_SUPPORT)
				}
#endif // defined(CHINESE_SUPPORT)

				nReturn = 1;
			}
			break;

		case KEY_END:			// 将当前插入位置放置在最后
			{

#if defined (CHINESE_SUPPORT)
				// 如果IME处于关闭状态，或者IME窗口中没有需要编辑的字符，则处理此消息
				if (! ((m_cIMEStatus == IME_OPEN) && bIMECanHandleControlKey))
				{
#endif // defined(CHINESE_SUPPORT)

					SetSel (GetTextLength(), GetTextLength());

#if defined (CHINESE_SUPPORT)
				}
#endif // defined(CHINESE_SUPPORT)

				nReturn = 1;
			}
			break;

		case KEY_BACK_SPACE:	// 删除当前插入位置前面的字符
			{

#if defined (CHINESE_SUPPORT)
				// 如果IME处于打开状态，且可以处理控制按键，则将此消息发送给IME窗口
				if ((m_cIMEStatus == IME_OPEN) && bIMECanHandleControlKey)
				{
					O_MSG msg;
					msg.pWnd	= m_pIME;
					msg.message = OM_KEYDOWN;
					msg.wParam	= KEY_BACK_SPACE;
					msg.lParam	= 0;
					m_pApp->PostMsg (&msg);
				}
				else
				{
#endif // defined(CHINESE_SUPPORT)

					int nStartPos = m_dwAddData1;
					int nEndPos   = m_dwAddData2;
					if ((nStartPos != -1) && (nEndPos != -1))
					{
						// 如果是插入点，则删除前面那个字符
						// 如果是选择区域，则删除选中的内容
						if (nStartPos == nEndPos)
						{
							DelOneCharacter (FALSE);
						}
						else
						{
							DelCurSel ();
						}
					}

#if defined (CHINESE_SUPPORT)
				}
#endif // defined(CHINESE_SUPPORT)

				nReturn = 1;
			}
			break;

		case KEY_DELETE:		// 删除当前插入位置后面的字符
			{

#if defined (CHINESE_SUPPORT)
				// 如果IME处于关闭状态，或者IME窗口中没有需要编辑的字符，则处理此消息
				if (! ((m_cIMEStatus == IME_OPEN) && bIMECanHandleControlKey))
				{
#endif // defined(CHINESE_SUPPORT)

					int nStartPos = m_dwAddData1;
					int nEndPos   = m_dwAddData2;
					if ((nStartPos != -1) && (nEndPos != -1))
					{
						// 如果是插入点，则删除后面那个字符
						// 如果是选择区域，则删除选中的内容
						if (nStartPos == nEndPos)
						{
							DelOneCharacter (TRUE);
						}
						else
						{
							DelCurSel ();
						}
					}

#if defined (CHINESE_SUPPORT)
				}
#endif // defined(CHINESE_SUPPORT)

				nReturn = 1;
			}
			break;

		case KEY_IME_ONOFF:		// 打开关闭输入法
			{

#if defined (CHINESE_SUPPORT)
				// cIMEStatus输入法状态：0禁用输入法；1输入法未开；2输入法已开
				if (m_cIMEStatus == IME_CLOSE)
				{
					// 打开IME
					if (m_pApp->OpenIME(this)) {
						m_cIMEStatus = IME_OPEN;
					}

					m_pIME = m_pApp->m_pIMEWnd;
				}
				else if (m_cIMEStatus == IME_OPEN)
				{
					// 关闭IME
					if (m_pApp->CloseIME(this)) {
						m_cIMEStatus = IME_CLOSE;
					}

					m_pIME = NULL;
				}
				nReturn = 0;

#endif // defined(CHINESE_SUPPORT)

			}
			break;

		case KEY_IME_NEXT:
		case KEY_IME_PREV:		// 切换输入法
			{

#if defined (CHINESE_SUPPORT)
				// 如果IME处于打开状态，则将此消息发送给IME窗口
				if (m_cIMEStatus == IME_OPEN)
				{
					O_MSG msg;
					msg.pWnd	= m_pIME;
					msg.message = OM_KEYDOWN;
					msg.wParam	= wParam;
					msg.lParam	= 0;
					m_pApp->PostMsg (&msg);
				}
				nReturn = 1;
#endif // defined(CHINESE_SUPPORT)

			}
			break;

		case KEY_ENTER:			// 传送给输入法的确认键
			{

#if defined (CHINESE_SUPPORT)
				// 如果IME处于打开状态，则将此消息发送给IME窗口
				// 如果IME未打开，向父窗口发送KEY_TAB消息
				if (m_cIMEStatus == IME_OPEN)
				{
					O_MSG msg;
					msg.pWnd    = m_pIME;
					msg.message = OM_KEYDOWN;
					msg.wParam  = KEY_ENTER;
					msg.lParam  = 0;
					m_pApp->PostMsg (&msg);
					nReturn = 1;
				}
				else
				{
#endif // defined(CHINESE_SUPPORT)

					if (IsWindowEnabled() &&
					    ((m_wStyle  & WND_STYLE_IGNORE_ENTER) == 0))
					{ 
						O_MSG msg;
						msg.pWnd    = m_pParent;
						msg.message = OM_KEYDOWN;
						msg.wParam  = KEY_TAB;
						msg.lParam  = 0;
						m_pApp->PostMsg (&msg);
						nReturn = 1;
					}
					else {
						nReturn = 0;
					}

#if defined (CHINESE_SUPPORT)
				}
#endif // defined(CHINESE_SUPPORT)

			}
			break;

		case KEY_0:				// 数字字母和小数点
		case KEY_1:
		case KEY_2:
		case KEY_3:
		case KEY_4:
		case KEY_5:
		case KEY_6:
		case KEY_7:
		case KEY_8:
		case KEY_9:
		case KEY_A:
		case KEY_B:
		case KEY_C:
		case KEY_D:
		case KEY_E:
		case KEY_F:
		case KEY_G:
		case KEY_H:
		case KEY_I:
		case KEY_J:
		case KEY_K:
		case KEY_L:
		case KEY_M:
		case KEY_N:
		case KEY_O:
		case KEY_P:
		case KEY_Q:
		case KEY_R:
		case KEY_S:
		case KEY_T:
		case KEY_U:
		case KEY_V:
		case KEY_W:
		case KEY_X:
		case KEY_Y:
		case KEY_Z:
		case KEY_SPACE:
		case KEY_PERIOD:
		case KEY_SUB:
		case KEY_EQUALS:
		case KEY_SEMICOLON:
		case KEY_QUOTATION:
		case KEY_BACK_QUOTE:
		case KEY_BACK_SLASH:
		case KEY_DIVIDE:
		case KEY_COMMA:
		case KEY_OPEN_BRACKET:
		case KEY_CLOSE_BRACKET:
			{

#if defined (CHINESE_SUPPORT)
				// 如果IME处于打开状态，则将此消息发送给IME窗口
				// 如果IME未打开，如果当前插入位置有效，则将字符插入
				if (m_cIMEStatus == IME_OPEN)
				{
					O_MSG msg;
					msg.pWnd	= m_pIME;
					msg.message = OM_KEYDOWN;
					msg.wParam	= wParam;
					msg.lParam	= 0;
					m_pApp->PostMsg (&msg);
					nReturn = 1;
				}
				else
				{
#endif // defined(CHINESE_SUPPORT)

					// 插入字符
					char sInsert [2];
					memset (sInsert, 0x0, sizeof(sInsert));

					if (lParam & SHIFT_MASK)
					{
						// 上档键有效
						switch (wParam) {
						case KEY_1:		sInsert[0] = '!';	break;
						case KEY_2:		sInsert[0] = '@';	break;
						case KEY_3:		sInsert[0] = '#';	break;
						case KEY_4:		sInsert[0] = '$';	break;
						case KEY_5:		sInsert[0] = '%';	break;
						case KEY_6:		sInsert[0] = '^';	break;
						case KEY_7:		sInsert[0] = '&';	break;
						case KEY_8:		sInsert[0] = '*';	break;
						case KEY_9:		sInsert[0] = '(';	break;
						case KEY_0:		sInsert[0] = ')';	break;
						case KEY_SPACE:	        sInsert[0] = ' ';	break;
						case KEY_PERIOD:        sInsert[0] = '>';	break;
						case KEY_SUB:           sInsert[0] = '_';	break;
						case KEY_EQUALS:        sInsert[0] = '+';	break;
						case KEY_SEMICOLON:     sInsert[0] = ':';	break;
						case KEY_QUOTATION:     sInsert[0] = '\"';	break;
						case KEY_BACK_QUOTE:    sInsert[0] = '~';	break;
						case KEY_BACK_SLASH:    sInsert[0] = '|';	break;
						case KEY_DIVIDE:        sInsert[0] = '?';	break;
						case KEY_COMMA:         sInsert[0] = '<';	break;
						case KEY_OPEN_BRACKET:  sInsert[0] = '{';	break;
						case KEY_CLOSE_BRACKET: sInsert[0] = '}';	break;
						}
					}
					else
					{
						switch (wParam) {
						case KEY_1:		sInsert[0] = '1';	break;
						case KEY_2:		sInsert[0] = '2';	break;
						case KEY_3:		sInsert[0] = '3';	break;
						case KEY_4:		sInsert[0] = '4';	break;
						case KEY_5:		sInsert[0] = '5';	break;
						case KEY_6:		sInsert[0] = '6';	break;
						case KEY_7:		sInsert[0] = '7';	break;
						case KEY_8:		sInsert[0] = '8';	break;
						case KEY_9:		sInsert[0] = '9';	break;
						case KEY_0:		sInsert[0] = '0';	break;
						case KEY_SPACE:	        sInsert[0] = ' ';	break;
						case KEY_PERIOD:        sInsert[0] = '.';	break;
						case KEY_SUB:           sInsert[0] = '-';	break;
						case KEY_EQUALS:        sInsert[0] = '=';	break;
						case KEY_SEMICOLON:     sInsert[0] = ';';	break;
						case KEY_QUOTATION:     sInsert[0] = '\'';	break;
						case KEY_BACK_QUOTE:    sInsert[0] = '`';	break;
						case KEY_BACK_SLASH:    sInsert[0] = '\\';	break;
						case KEY_DIVIDE:        sInsert[0] = '/';	break;
						case KEY_COMMA:         sInsert[0] = ',';	break;
						case KEY_OPEN_BRACKET:  sInsert[0] = '[';	break;
						case KEY_CLOSE_BRACKET: sInsert[0] = ']';	break;
						}
					}

					if (0x0 == sInsert[0])
					{
						// 处理字母键
						if (  ((lParam & SHIFT_MASK) && ! (lParam & CAPSLOCK_MASK))
							||(! (lParam & SHIFT_MASK) && (lParam & CAPSLOCK_MASK)))
						{
							// 非大写字母状态按下shift，或者大写字母状态未按下shift
							// 输入大写字母
							sInsert[0] = (char)wParam;
						}
						else
						{
							// 小写字母的ASCII值比大写字母大
							sInsert[0] = (char)wParam + 32;
						}
					}

					InsertCharacter (sInsert);
					
					RenewLeftPos ();	// 更改显示区左端第一个字符的索引
					RenewCaret ();		// 更新脱字符信息
					nReturn = 1;

#if defined (CHINESE_SUPPORT)
				}
#endif // defined(CHINESE_SUPPORT)

			}
			break;
		case KEY_00:
			{

#if defined (CHINESE_SUPPORT)
				if (m_cIMEStatus != IME_OPEN)
				{
#endif // defined(CHINESE_SUPPORT)

					// 插入字符
					char sInsert [3];
					sInsert[0] = '0';
					sInsert[1] = '0';
					sInsert[2] = 0x0;

					InsertCharacter (sInsert);
					
					RenewLeftPos ();	// 更改显示区左端第一个字符的索引
					RenewCaret ();		// 更新脱字符信息
					nReturn = 1;

#if defined (CHINESE_SUPPORT)
				}
#endif // defined(CHINESE_SUPPORT)

			}
			break;

		default:
			{
				// 纯英文环境下，如果禁用输入法，则不允许输入英文字母
				if ((m_wStyle & WND_STYLE_DISABLE_IME) > 0)
				{
					nReturn = 1;
					break;
				}

#if defined (CHINESE_SUPPORT)
				// 输入其他英文字母
				// 如果IME处于打开状态，则将此消息发送给IME窗口
				// 如果IME未打开，如果当前插入位置有效，则将字符插入
				if (m_cIMEStatus == IME_OPEN)
				{
					O_MSG msg;
					msg.pWnd	= m_pIME;
					msg.message = OM_KEYDOWN;
					msg.wParam	= wParam;
					msg.lParam	= 0;
					m_pApp->PostMsg (&msg);
					nReturn = 1;
				}
				else
				{
#endif // defined(CHINESE_SUPPORT)

					// 插入字符
					char sInsert [2];
					memset (sInsert, 0x0, sizeof(sInsert));

					if( VKToASC (sInsert, wParam, lParam))
					{
						InsertCharacter (sInsert);
						RenewLeftPos ();	// 更改显示区左端第一个字符的索引
						RenewCaret ();		// 更新脱字符信息
						nReturn = 1;
					}

#if defined (CHINESE_SUPPORT)
				}
#endif // defined(CHINESE_SUPPORT)

			}
		}
	}
	else if (nMsg == OM_SETFOCUS)
	{
		// 得到焦点，应当将选择范围设置为全选，并设置相应的脱字符
		if (pWnd == this)
		{
			m_dwAddData1 = 0;
			m_dwAddData2 = GetTextLength ();
			RenewLeftPos ();	// 更改显示区左端第一个字符的索引
			RenewCaret ();		// 更新脱字符信息
		}

#if defined (CHINESE_SUPPORT)
		// 自动打开输入法窗口
		if ((m_wStyle & WND_STYLE_AUTO_OPEN_IME) > 0)
		{
			if (m_cIMEStatus == IME_CLOSE)
			{
				if (m_pApp->OpenIME(this)) {
					m_cIMEStatus = IME_OPEN;
				}

				m_pIME = m_pApp->m_pIMEWnd;
			}
		}
#endif // defined(CHINESE_SUPPORT)

	}
	else if (nMsg == OM_KILLFOCUS)
	{
		// 失去焦点，恢复脱字符，如果输入法处于打开状态则关闭输入法
		if (pWnd == this)
		{

#if defined (CHINESE_SUPPORT)
			if (m_cIMEStatus == IME_OPEN)
			{
				m_cIMEStatus = IME_CLOSE;
				m_pApp->CloseIME(this);
			}
#endif // defined(CHINESE_SUPPORT)

			m_Caret.bValid = FALSE;
			m_Caret.bShow  = FALSE;
			m_pApp->SetCaret (&m_Caret);
		}
	}
	else if (nMsg == OM_CHAR)
	{
		// 处理输入法窗口发回的字符
		if (pWnd == this)
		{
			// 插入字符
			char sInsert [3];
			memset (sInsert, 0x0, sizeof(sInsert));

			sInsert[0] = (BYTE)wParam;
			if (wParam > 127)	// 汉字
				sInsert[1] = lParam;

			InsertCharacter (sInsert);
			
			RenewLeftPos ();	// 更改显示区左端第一个字符的索引
			RenewCaret ();		// 更新脱字符信息
		}
	}

	UpdateView (this);
	return nReturn;
}

#if defined (MOUSE_SUPPORT)
// 坐标设备消息处理
int OEdit::PtProc (OWindow* pWnd, int nMsg, int wParam, int lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int nReturn = OWindow::PtProc (pWnd, nMsg, wParam, lParam);

	// 鼠标左键消息
	if (nMsg == OM_LBUTTONDOWN)
	{
		if (PtInWindow (wParam, lParam))
		{
			if (IsWindowEnabled())
			{
				// 如果本窗口不是焦点，则将本窗口设置为焦点
				if (m_pParent != this)
				{
					O_MSG msg;
					msg.pWnd    = m_pParent;
					msg.message = OM_SETCHILDACTIVE;
					msg.wParam  = m_ID;
					msg.lParam  = 0;
					m_pApp->SendMsg (&msg);
				}

				// 改变当前插入点的位置
				int nCurPos = PtInItems(wParam);
				if (nCurPos != -1) {
					SetSel (nCurPos, nCurPos);
				}

				// 设置托拽处理标志
				m_nMouseMoving = 1;
				m_nOldPos = nCurPos;
			}
			nReturn = 1;
		}
	}
	else if (nMsg == OM_LBUTTONUP)
	{
		if (m_nMouseMoving == 1)
		{
			// 鼠标左键弹起，取消托拽
			m_nMouseMoving = 0;
			m_nOldPos = 0;
			nReturn = 1;
		}
	}
	else if (nMsg == OM_MOUSEMOVE)
	{
		if (m_nMouseMoving == 1)
		{
			// 处理托拽
			int nCurPos = PtInItems(wParam);

			int nLeftPos  = m_dwAddData4;
			int nRightPos = GetRightDisplayIndex ();
			if (nCurPos > nRightPos)
			{
				// 左端位置需要向右移动
				nLeftPos = nLeftPos + (nCurPos - nRightPos);

				// 如果当前位置不合理，则将当前位置向右移动一个字节
				if (! IsPosValid (m_sCaption, nLeftPos))
					nLeftPos += 1;

				m_dwAddData4 = nLeftPos;
			}

			SetSel (m_nOldPos, nCurPos);
			RenewCaret();
			nReturn = 1;
		}
	}

	UpdateView (this);
	return nReturn;
}

// 测试坐标落于的位置，该测试不关心y值
int OEdit::PtInItems (int x)
{
	CHECK_TYPE_RETURN;

	// 选择到边界之左，则选择到最前
	if (x < m_x) {
		return 0;
	}

	// 选择到边界之右，则选择到最后
	if (x > m_x + m_w) {
		return GetTextLength();
	}

	int nPointX   = x - m_x - 2;
	int nCurPos   = 0;
	int nLeftPos  = m_dwAddData4;
	int nRightPos = GetRightDisplayIndex ();

	if (nPointX > 1)
	{
		// 逐个检查合理位置
		char* pString = &(m_sCaption[nLeftPos]);

		int nStrLength = nRightPos - nLeftPos;
		int nDisplayLength = 0;
		int nValidPos = 0;

		int nDistence1 = 0;
		int nDistence2 = 0;
		int i;
		for (i = 0; i < nStrLength; i++)
		{
			if (IsChinese (pString, nValidPos, TRUE))
			{
				nValidPos += 2;
				nDisplayLength += (HZK_W + 1);

				nDistence2 = nDisplayLength;
				if ((nDistence1 <= nPointX) && (nDistence2 >= nPointX))
				{
					nCurPos = nValidPos;
					if ((nPointX - nDistence1) < (nDistence2 - nPointX))
						nCurPos -= 2;
					break;
				}
			}
			else
			{
				nValidPos ++;
				nDisplayLength += (ASC_W + 1);
				nDistence2 = nDisplayLength;
				if ((nDistence1 <= nPointX) && (nDistence2 >= nPointX))
				{
					nCurPos = nValidPos;
					if ((nPointX - nDistence1) < (nDistence2 - nPointX))
						nCurPos --;
					break;
				}
			}

			if (nValidPos >= nStrLength)
			{
				nCurPos = nStrLength;
				break;
			}

			nDistence1 = nDistence2;
		}
	}

	return nCurPos + nLeftPos;
}
#endif // defined(MOUSE_SUPPORT)

// 设置当前选择区域的起始位置和终止位置
// 如果位置跨越了汉字，则向后推一个字节
BOOL OEdit::SetSel (int nStart, int nEnd)
{
	CHECK_TYPE_RETURN;

	// 如果Start比End大，则交换
	if (nStart > nEnd)
	{
		int nTemp = nEnd;
		nEnd = nStart;
		nStart = nTemp;
	}

	if (! IsPosValid (m_sCaption, nStart))
		nStart ++;

	if (! IsPosValid (m_sCaption, nEnd))
		nEnd ++;

	if (nEnd > GetTextLength ())
		nEnd = GetTextLength ();

	if (nStart < 0)
		nStart = 0;

	if (nEnd < nStart)
		nEnd = nStart;

	m_dwAddData1 = nStart;
	m_dwAddData2 = nEnd;

	RenewLeftPos ();	// 更改显示区左端第一个字符的索引
	RenewCaret ();		// 更新脱字符信息
	return TRUE;
}

// 获得当前选择区域的起始位置和终止位置
BOOL OEdit::GetSel (int* pnStart, int* pnEnd)
{
	CHECK_TYPE_RETURN;

	int nStartPos = m_dwAddData1;
	int nEndPos   = m_dwAddData2;

	if ((nStartPos == -1) || (nEndPos == -1))
		return FALSE;

	*pnStart = nStartPos;
	*pnEnd   = nEndPos;
	return TRUE;
}

// 向当前位置插入字符串
// 如果当前位置是一个选择区，则替换当前选择区域的字符串，
// 然后将选择区修改成一个插入位置
// 如果当前位置是一个插入位置，则在此插入位置上插入字符串
// 注意，如果总长度超越了长度限制，则截取合适长度的字串
// 截取时应注意汉字的处理
BOOL OEdit::InsertCharacter (char* sString)
{
	CHECK_TYPE_RETURN;

	// 选择区域的开始位置和终止位置
	int nStartPos   = m_dwAddData1;
	int nEndPos     = m_dwAddData2;

	if ((nStartPos == -1) || (nEndPos == -1)) {
		return FALSE;
	}

	if (nStartPos > nEndPos) {
		return FALSE;
	}

	// 根据长度判断可以插入多少个字符
	int nLengthLimit = m_dwAddData3;
	int nInsertCount = nLengthLimit-(GetTextLength()-(nEndPos-nStartPos));

	// 如果当前EDIT中的字符串长度已经超过了iLengthLimit，
	// 则iInsertCount会变成负数，会导致程序出错
	if (nInsertCount <= 0) {
		return FALSE;
	}

	// 从输入字符串中截取适当的长度(注意判断汉字！)
	char sTemp [WINDOW_CAPTION_BUFFER_LEN];
	memset (sTemp, 0x0, sizeof(sTemp));
	strncpy (sTemp, sString, nInsertCount);
	int nInsertLength = strlen (sTemp);

	if (nInsertLength == 1 && (BYTE)sTemp[0] >127) {
		return TRUE;
	}

	if (! IsPosValid (sTemp, nInsertLength)) {// 如果截断了汉字，则减掉一个字节
		nInsertLength --;
	}

	if (nInsertLength <= 0) {     // 插入长度为0，不修改当前选区的长度，直接返回
		return TRUE;
	}

	// 插入字符串并修改当前插入点的位置
	char sTemp2 [WINDOW_CAPTION_BUFFER_LEN];
	memset (sTemp2, 0x0, sizeof(sTemp2));
	int nOldLength = GetTextLength();
	strncpy (sTemp2, m_sCaption, nStartPos);			// 拷贝出插入点以前的部分
	strncpy ((sTemp2+nStartPos), sTemp, nInsertLength);	// 将插入的部分连接在后面
	strncpy ((sTemp2+nStartPos+nInsertLength),
			 (m_sCaption+nEndPos),
			 (nOldLength-nEndPos));						// 将插入位置以后的部分接在最后
	memset (m_sCaption, 0x0, WINDOW_CAPTION_BUFFER_LEN);
	strncpy (m_sCaption, sTemp2, nOldLength+nInsertLength);

	// 设置当前插入点
	m_dwAddData1 = nStartPos + nInsertLength;
	m_dwAddData2 = m_dwAddData1;

	RenewLeftPos ();	// 更改显示区左端第一个字符的索引
	RenewCaret ();		// 更新脱字符信息
	return TRUE;
}

// 删除当前位置前面的一个字符或者后面的一个字符
// bMode: TRUE,删后面的;FALSE,删前面的
BOOL OEdit::DelOneCharacter (BOOL bMode)
{
	CHECK_TYPE_RETURN;

	// 选择区域的开始位置和终止位置
	int nStartPos	= m_dwAddData1;
	int nEndPos		= m_dwAddData2;

	if ((nStartPos == -1) || (nEndPos == -1)) {
		return FALSE;
	}

	if (nStartPos != nEndPos) {	// 不是插入点，退出
		return FALSE;
	}

	if (bMode)
	{
		// 删除插入点后面一个字符
		int nDelLen = 0;
		if (IsChinese (m_sCaption, nStartPos, TRUE))
			nDelLen = 2;
		else
			nDelLen = 1;

		if ((nStartPos+nDelLen) > GetTextLength())
			return FALSE;

		m_dwAddData2 = nStartPos + nDelLen;

		if (DelCurSel ())
			return TRUE;
		else
			return FALSE;
	}
	else
	{
		// 删除插入点前面一个字符
		int nDelLen = 0;
		if (IsChinese (m_sCaption, nStartPos, FALSE))
			nDelLen = 2;
		else
			nDelLen = 1;

		if ((nStartPos-nDelLen) < 0)
			return FALSE;

		m_dwAddData1 = nStartPos - nDelLen;

		if (DelCurSel ())
			return TRUE;
		else
			return FALSE;
	}

	RenewLeftPos ();	// 更改显示区左端第一个字符的索引
	RenewCaret ();		// 更新脱字符信息

}

// 删除当前选中区域的内容
BOOL OEdit::DelCurSel ()
{
	CHECK_TYPE_RETURN;

	// 选择区域的开始位置和终止位置
	int nStartPos	= m_dwAddData1;
	int nEndPos		= m_dwAddData2;

	if ((nStartPos == -1) || (nEndPos == -1)) {
		return FALSE;
	}

	if (nStartPos >= nEndPos) {	// 没必要删除，直接退出
		return TRUE;
	}

	// 插入字符串并修改当前插入点的位置
	char sTemp2 [WINDOW_CAPTION_BUFFER_LEN];
	memset (sTemp2, 0x0, sizeof(sTemp2));
	int nOldLength = GetTextLength();
	strncpy (sTemp2, m_sCaption, nStartPos);			// 拷贝出插入点以前的部分
	strncpy ((sTemp2+nStartPos),
			 (m_sCaption+nEndPos),
			 (nOldLength-nEndPos));						// 将插入位置以后的部分接在最后
	memset (m_sCaption, 0x0, WINDOW_CAPTION_BUFFER_LEN);
	strncpy (m_sCaption, sTemp2, nOldLength-(nEndPos-nStartPos));

	// 设置当前插入点
	m_dwAddData1 = nStartPos;
	m_dwAddData2 = nStartPos;

	RenewLeftPos ();	// 更改显示区左端第一个字符的索引
	RenewCaret ();		// 更新脱字符信息
	return TRUE;
}

// 限制输入字符串的最大长度
BOOL OEdit::LimitText (int nLength)
{
	CHECK_TYPE_RETURN;

	if (nLength > WINDOW_CAPTION_BUFFER_LEN-1) {
		return FALSE;
	}

	m_dwAddData3 = nLength;
	return TRUE;
}

// 清空字符串的内容
BOOL OEdit::Clean ()
{
	CHECK_TYPE_RETURN;

	memset (m_sCaption, 0x0, WINDOW_CAPTION_BUFFER_LEN);
	m_dwAddData1 = 0;
	m_dwAddData2 = 0;
	m_dwAddData4 = 0;
	RenewLeftPos ();	// 更改显示区左端第一个字符的索引
	RenewCaret ();

	return TRUE;
}

// 返回输入法窗口的状态
BOOL OEdit::IsIMEOpen()
{
	CHECK_TYPE_RETURN;

    if (m_cIMEStatus == IME_OPEN) {
		return TRUE;
	}

    return FALSE;
}

// 更改显示区左端第一个字符的索引
void OEdit::RenewLeftPos ()
{
	CHECK_TYPE;

	// 如果当前脱字符超出了编辑框的矩形范围，则应当更新显示区域的位置
	int nStartPos = m_dwAddData1;
	int nEndPos   = m_dwAddData2;
	int nLeftPos  = m_dwAddData4;
	int nRightPos = GetRightDisplayIndex ();

	// 如果脱字符可见，则不进行处理
	if ((nStartPos >= nLeftPos) && (nStartPos <= nRightPos)) {
		return;
	}

	int nNewLeftPos = 0;
	if (nStartPos < nLeftPos)
	{
		// 左端位置需要向左移动
		nNewLeftPos = nStartPos;
	}
	else if (nStartPos > nRightPos)
	{
		// 左端位置需要向右移动
		nNewLeftPos = nLeftPos + (nEndPos - nRightPos);
		if (nNewLeftPos > nStartPos)
			nNewLeftPos = nStartPos;
	}

	// 如果当前位置不合理，则将当前位置向右移动一个字节
	if (! IsPosValid (m_sCaption, nNewLeftPos)) {
		nNewLeftPos += 1;
	}
	
	m_dwAddData4 = nNewLeftPos;
}

// 根据当前的脱字符设置更新系统脱字符
void OEdit::RenewCaret ()
{
	CHECK_TYPE;

	// 如果本控件是焦点，才进行此操作
	if (! IsWindowActive()) {
		return;
	}

	int nStartPos = m_dwAddData1;
	int nEndPos   = m_dwAddData2;
	int nLeftPos  = m_dwAddData4;

	// 根据编辑框矩形尺寸算出当前显示字符串右端的位置
	int nRightPos = GetRightDisplayIndex ();

	if (nStartPos == nEndPos)
	{
		// 插入位置是一个点，设置成闪烁的竖线
		// 如果脱字符位置超越矩形区域，不显示
		if ((nStartPos < nLeftPos) || (nStartPos > nRightPos))
		{
			m_Caret.bValid = FALSE;
			m_pApp->SetCaret (&m_Caret);
			return;
		}

		// 计算从插入点到脱字符的长度(像素)
		int nLength = GetDisplayLength ((m_sCaption+nLeftPos), (nStartPos-nLeftPos));

		m_Caret.x = m_x+1+nLength;
		m_Caret.y = m_y+1;
		m_Caret.w = 2;
		m_Caret.h = m_h - 2;
		m_Caret.bFlash = TRUE;
		m_Caret.bShow  = TRUE;
		m_Caret.lTimeInterval = 500;
		m_Caret.bValid = TRUE;
	}
	else
	{
		// 插入位置是一个区域，设置成反白的区域
		// 根据边界修改脱字符的开始位置和终止位置
		if (nStartPos < nLeftPos) {
			nStartPos = nLeftPos;
		}

		if (nEndPos > nRightPos) {
			nEndPos = nRightPos;
		}

		if (nEndPos < nStartPos)
		{
			m_Caret.bValid = FALSE;
			m_pApp->SetCaret (&m_Caret);
			return;
		}

		// 计算脱字符左端的位置(像素)
		int nLeft = GetDisplayLength ((m_sCaption+nLeftPos), (nStartPos-nLeftPos));

		// 计算脱字符右端的位置(像素)
		int nRight = GetDisplayLength ((m_sCaption+nLeftPos), (nEndPos-nLeftPos));

		m_Caret.x = m_x+2+nLeft;
		m_Caret.y = m_y+2;
		m_Caret.w = nRight-nLeft;
		m_Caret.h = m_h - 4;
		m_Caret.bFlash = FALSE;
		m_Caret.bShow  = TRUE;
		m_Caret.lTimeInterval = 2000;
		m_Caret.bValid = TRUE;
	}

	m_pApp->SetCaret (&m_Caret);
}

// 取得当前显示区域最右端字符的索引
int OEdit::GetRightDisplayIndex ()
{
	CHECK_TYPE_RETURN;

	int nStartPos = m_dwAddData1;
	int nEndPos   = m_dwAddData2;
	int nLeftPos  = m_dwAddData4;

	int nRightPos = nLeftPos;
	int nCheckLen = 0;
	int nDisplayLength = 0;
	int nStringLength  = GetTextLength ();

	while (nDisplayLength < (m_w - 4))	// 4,左右边距
	{
		nRightPos = nLeftPos + nCheckLen;
		
		if (IsChinese (m_sCaption, (nLeftPos+nCheckLen), TRUE))
			nCheckLen += 2;
		else
			nCheckLen += 1;

		if ((nLeftPos + nCheckLen) > nStringLength)
			break;

		nDisplayLength = GetDisplayLength ((m_sCaption+nLeftPos), nCheckLen);
	}
	return nRightPos;
}

// 将字母键转换成ASC码
BOOL OEdit::VKToASC (char* psString, int nVK, int nMask)
{
	CHECK_TYPE_RETURN;

#if !defined (CHINESE_SUPPORT)

	if ((nMask & CAPSLOCK_MASK) > 0)
	{
		// 输入大写字母
		switch (nVK)
		{
		case KEY_A:    psString[0] = 'A';    break;
		case KEY_B:    psString[0] = 'B';    break;
		case KEY_C:    psString[0] = 'C';    break;
		case KEY_D:    psString[0] = 'D';    break;
		case KEY_E:    psString[0] = 'E';    break;
		case KEY_F:    psString[0] = 'F';    break;
		case KEY_G:    psString[0] = 'G';    break;
		case KEY_H:    psString[0] = 'H';    break;
		case KEY_I:    psString[0] = 'I';    break;
		case KEY_J:    psString[0] = 'J';    break;
		case KEY_K:    psString[0] = 'K';    break;
		case KEY_L:    psString[0] = 'L';    break;
		case KEY_M:    psString[0] = 'M';    break;
		case KEY_N:    psString[0] = 'N';    break;
		case KEY_O:    psString[0] = 'O';    break;
		case KEY_P:    psString[0] = 'P';    break;
		case KEY_Q:    psString[0] = 'Q';    break;
		case KEY_R:    psString[0] = 'R';    break;
		case KEY_S:    psString[0] = 'S';    break;
		case KEY_T:    psString[0] = 'T';    break;
		case KEY_U:    psString[0] = 'U';    break;
		case KEY_V:    psString[0] = 'V';    break;
		case KEY_W:    psString[0] = 'W';    break;
		case KEY_X:    psString[0] = 'X';    break;
		case KEY_Y:    psString[0] = 'Y';    break;
		case KEY_Z:    psString[0] = 'Z';    break;
		default:       return FALSE;
		}
	}
	else
	{
		// 输入小写字母
		switch( nVK )
		{
		case KEY_A:    psString[0] = 'a';    break;
		case KEY_B:    psString[0] = 'b';    break;
		case KEY_C:    psString[0] = 'c';    break;
		case KEY_D:    psString[0] = 'd';    break;
		case KEY_E:    psString[0] = 'e';    break;
		case KEY_F:    psString[0] = 'f';    break;
		case KEY_G:    psString[0] = 'g';    break;
		case KEY_H:    psString[0] = 'h';    break;
		case KEY_I:    psString[0] = 'i';    break;
		case KEY_J:    psString[0] = 'j';    break;
		case KEY_K:    psString[0] = 'k';    break;
		case KEY_L:    psString[0] = 'l';    break;
		case KEY_M:    psString[0] = 'm';    break;
		case KEY_N:    psString[0] = 'n';    break;
		case KEY_O:    psString[0] = 'o';    break;
		case KEY_P:    psString[0] = 'p';    break;
		case KEY_Q:    psString[0] = 'q';    break;
		case KEY_R:    psString[0] = 'r';    break;
		case KEY_S:    psString[0] = 's';    break;
		case KEY_T:    psString[0] = 't';    break;
		case KEY_U:    psString[0] = 'u';    break;
		case KEY_V:    psString[0] = 'v';    break;
		case KEY_W:    psString[0] = 'w';    break;
		case KEY_X:    psString[0] = 'x';    break;
		case KEY_Y:    psString[0] = 'y';    break;
		case KEY_Z:    psString[0] = 'z';    break;
		default:       return FALSE;
		}
	}

	psString[1] = 0;
	return TRUE;

#endif //!defined(CHINESE_SUPPORT)

#if defined (CHINESE_SUPPORT)
	return FALSE;
#endif //defined( CHINESE_SUPPORT )

}

/* END */