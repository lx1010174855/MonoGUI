// OMsgQueue.h: interface for the OMsgQueue class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined(__OMSGQUEUE_H__)
#define __OMSGQUEUE_H__

typedef struct _O_MSG
{
	class OWindow*	pWnd;
	int				message;
	int				wParam;
	int				lParam;
} O_MSG;

#define MSG_QUEUE_SIZE    (MESSAGE_MAX+1)//循环队列浪费1个单元

class OMsgQueue
{
private:
	int  m_nFront;
	int  m_nRear;
	O_MSG m_arMsgQ[MSG_QUEUE_SIZE];

public:
	OMsgQueue ();
	virtual ~OMsgQueue ();

	// 从消息队列中获取一条消息，取出的消息将被删除
	// 如果是OM_QUIT消息，则返回0，消息队列空了返回-1，其他情况返回1；
	int GetMsg (O_MSG* pMsg);

	// 向消息队列中添加一条消息；
	// 如果消息队列满（消息数量达到了MESSAGE_MAX 所定义的数目），则返回失败；
	BOOL PostMsg (O_MSG* pMsg);

	// 在消息队列中查找指定类型的消息；
	// 如果发现消息队列中有指定类型的消息，则返回TRUE；
	// 该函数主要用在定时器处理上。CheckTimer函数首先检查消息队列中有没有相同的定时器消息，如果没有，再插入新的定时器消息
	BOOL FindMsg (O_MSG* pMsg);

	// 清除消息队列中所有的消息
	BOOL RemoveAll ();
};

#endif // !defined(__OMSGQUEUE_H__)
