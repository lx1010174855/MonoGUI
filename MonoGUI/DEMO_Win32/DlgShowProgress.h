// DlgShowProgress.h
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined (__DLGSHOWPROGRESS_H__)
#define __DLGSHOWPROGRESS_H__

class CDlgShowProgress : public ODialog
{
public:
	CDlgShowProgress();
	virtual ~CDlgShowProgress();

	// 初始化
	void Init();
};

#endif // !defined(__DLGSHOWPROGRESS_H__)